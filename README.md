# OpenML dataset: cacao_flavor

https://www.openml.org/d/42166

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Chocolate Bar Ratings.
    Expert ratings of over 1,700 chocolate bars. Each chocolate is evaluated from a combination of both objective qualities and subjective interpretation. A rating here only represents an experience with one bar from one batch. Batch numbers, vintages and review dates are included in the database when known.

    The database is narrowly focused on plain dark chocolate with an aim of appreciating the flavors of the cacao when made into chocolate. The ratings do not reflect health benefits, social missions, or organic status.
    
    Flavor is the most important component of the Flavors of Cacao ratings. Diversity, balance, intensity and purity of flavors are all considered. It is possible for a straight forward single note chocolate to rate as high as a complex flavor profile that changes throughout. Genetics, terroir, post harvest techniques, processing and storage can all be discussed when considering the flavor component.
    
    Texture has a great impact on the overall experience and it is also possible for texture related issues to impact flavor. It is a good way to evaluate the makers vision, attention to detail and level of proficiency.
    
    Aftermelt is the experience after the chocolate has melted. Higher quality chocolate will linger and be long lasting and enjoyable. Since the aftermelt is the last impression you get from the chocolate, it receives equal importance in the overall rating.
    
    Overall Opinion is really where the ratings reflect a subjective opinion. Ideally it is my evaluation of whether or not the components above worked together and an opinion on the flavor development, character and style. It is also here where each chocolate can usually be summarized by the most prominent impressions that you would remember about each chocolate.
    
    Flavors of Cacao Rating System:
    5= Elite (Transcending beyond the ordinary limits)
    4= Premium (Superior flavor development, character and style)
    3= Satisfactory(3.0) to praiseworthy(3.75) (well made with special qualities)
    2= Disappointing (Passable but contains at least one significant flaw)
    1= Unpleasant (mostly unpalatable)

    Acknowledgements
    These ratings were compiled by Brady Brelinski, Founding Member of the Manhattan Chocolate Society. For up-to-date information, as well as additional content (including interviews with craft chocolate makers), please see his website: http://flavorsofcacao.com/index.html

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42166) of an [OpenML dataset](https://www.openml.org/d/42166). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42166/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42166/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42166/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

